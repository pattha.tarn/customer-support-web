import './App.css'

import { DatePicker } from 'antd'

function App() {
  return (
    <>
      <div className="flex justify-end">
        <p className="text-red">Hello</p>
        <DatePicker />
      </div>
    </>
  )
}

export default App
